import {useEffect, useState} from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

/* Custom imports. */
import {
    useQuery,
    useMutation,
    useQueryClient,
    QueryClient,
    QueryClientProvider,
} from '@tanstack/react-query'
import Layout from "./layout/Layout";

function App(): JSX.Element {
  const [count, setCount] = useState(0)
    const queryClient = new QueryClient()

    // useEffect(()=>{
    //     try {
    //         CreateNewConnection()
    //     } catch (err) {
    //         console.log("err: ", err)
    //     }
    //
    // }, [])


    return (
    <QueryClientProvider client={queryClient}>
        <Layout/>
    </QueryClientProvider>
  )
}

export default App
