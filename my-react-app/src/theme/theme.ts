export const backgroundColors = {
    darkGreyBg: '#242424',
    lightBg: '#fefacb',
    blueBg: 'blue',
    redBg: 'red',
}

export const textColors = {
    textWhite: '#fff',
    textBlack: '#000',
}