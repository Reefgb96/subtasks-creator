// Custom imports.
import Header from "../ui/components/Layout/Header";
import Content from "../ui/components/Layout/Content";
import Footer from "../ui/components/Layout/Footer";
import {LayoutWrapper} from "../ui/components/Layout/LayoutWrappers";

const Layout = (): JSX.Element => {
    return (
        <LayoutWrapper>
            <Header/>
            <Content/>
            <Footer/>
        </LayoutWrapper>
    );
};

export default Layout;