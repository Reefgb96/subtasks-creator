import React, {ReactNode} from "react";

export type HeaderProps = {
    titleText: string;
}

export type InputProps = {
    inputText: string;
    labelText: string;
    children?: ReactNode
}

export type ContainerProps = {
    children?: ReactNode
}

export type SubmitBtnProps = {
    btnText: string;
    children?: ReactNode;
    onFormSend: Function;
}