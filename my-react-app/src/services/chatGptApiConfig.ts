import {Configuration, OpenAIApi} from "openai";

export const openAi = new OpenAIApi(new Configuration({
        apiKey: import.meta.env.REACT_APP_OPENAI_API_KEY
    }))

