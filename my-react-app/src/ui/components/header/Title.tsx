import React from 'react';
import {HeaderProps} from "../../../types/props";

const Title: React.FC<HeaderProps> = ({titleText}) => {
    return (
        <h1>
            {titleText}
        </h1>
    );
};

export default Title;