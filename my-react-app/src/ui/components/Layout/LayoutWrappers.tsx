import styled from "styled-components";
import {backgroundColors,textColors} from "../../../theme/theme";

export const LayoutWrapper = styled.div`
  width: 100vw;
  height: 100vh;
`;

export const HeaderWrapper = styled.nav`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 100vw;
  height: 10vh;
  display: flex;
  //flex-direction: column;
  justify-content: center;
  align-items: center;
  border-bottom: 1px solid #fff;
  font-size: 1.8rem;
  background-color: ${backgroundColors.darkGreyBg};
  color: ${textColors.textWhite};
`;