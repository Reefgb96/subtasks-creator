import React from 'react';
import {HeaderWrapper} from "./LayoutWrappers";
import Title from "../header/Title";


const Header = () => {
    return (
        <HeaderWrapper>
            <Title titleText="The Subtasks Creator"/>
        </HeaderWrapper>
    );
};

export default Header;