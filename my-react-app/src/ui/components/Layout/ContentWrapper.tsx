import styled from "styled-components";
import {backgroundColors} from "../../../theme/theme";

export const ContentWrapper = styled.section`
  width: 100%;
  height: 100%;
  display: flex;
  //flex-direction: column;
  justify-content: center;
  align-items: center;
`;