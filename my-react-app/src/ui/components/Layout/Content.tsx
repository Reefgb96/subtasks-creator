import React from 'react';
import {ContentWrapper} from "./ContentWrapper";
import FormWrapper from "../form/FormWrapper";

const Content = () => {

    return (
        <ContentWrapper>
            <FormWrapper/>

        </ContentWrapper>
    );
};

export default Content;