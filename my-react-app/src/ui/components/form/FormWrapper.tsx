import React, {FormEvent, useState} from 'react';
import {Form} from "./Form";
import Input from "./Input";
import {InputWrapper} from "./InputWrapper";
import SubmitBtn from "./SubmitBtn";
import {openAi} from "../../../services/chatGptApiConfig";


const FormWrapper = () => {
    const [output, setOutput] = useState<any | undefined>("");
    const [error, setError] = useState<any>([]);
    const [loading, setLoading] = useState<any>(null);
    const [isFetched, setIsFetched] = useState<boolean>(false);

    const FormSubmit = async (ev: FormEvent<HTMLFormElement>): Promise<any> => {
        console.log("click from parent element.")
        console.log("env: ", import.meta.env.VITE_OPENAI_API_KEY)
        let query = await openAi.createChatCompletion({
            model: 'gpt-3.5-turbo',
            messages: [{role: "user", content: "who is the best singer in the world?"}]
        }, {}).then(res => {
            console.log(res)
            setLoading(false)
            setIsFetched(true)
            // @ts-ignore
            setOutput(res.data.choices[0].message.content)
        }).catch(error =>{
            console.log(error)
            setError(error)
        })

        // return query
    }

    return (
        <Form onSubmit={FormSubmit}>
            <InputWrapper>
                <Input inputText="ex: Create Modal." labelText="Story Name:"/>
                <Input inputText="ex: On button click open modal." labelText="End Goal:"/>
                <Input inputText="ex: React.ts" labelText="Language and framework:"/>
            </InputWrapper>
            <SubmitBtn btnText="Create subtasks!" onFormSend={FormSubmit} />
        </Form>
    );
};

export default FormWrapper;
