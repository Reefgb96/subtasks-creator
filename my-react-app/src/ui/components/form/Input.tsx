import React, {ChangeEvent, LegacyRef, useRef, useState} from 'react';
import {InputProps} from "../../../types/props";
import Container from "../Container";

const Input = (props: InputProps): JSX.Element => {
    const [inputValue, setInputValue] = useState<string>("")
    let inputRef = useRef()

    const onInputChange = (e: ChangeEvent<HTMLInputElement>) => {
        // e.preventDefault()
        setInputValue(e.target.value)
    }

    return (
        <Container>
            <div style={{alignSelf: 'flex-start', margin: '0 10%'}}>
            <label htmlFor="input-id">{props.labelText}</label>
            </div>
            <input onChange={onInputChange}  value={inputValue} placeholder={props.inputText} id="input-id" style={{width:'80%',height:'40px', borderRadius:'6px', margin: '2% 0'}} >
                {props.children}
            </input>
        </Container>
    );
};

export default Input;
