import React from 'react';
import {SubmitBtnProps} from "../../../types/props";
import {openAi} from "../../../services/chatGptApiConfig";

const SubmitBtn = (props: SubmitBtnProps) => {

    const handleSubmitClick = async (ev: any) => {
        ev.preventDefault()
        props.onFormSend()
        console.log("env: ", import.meta.env.VITE_OPENAI_API_KEY)
        // let query = await openAi.createChatCompletion({
        //     model: 'gpt-3.5-turbo',
        //     messages: [{role: "user", content: "who is the best singer in the world?"}]
        // }, {}).then(res => {
        //     console.log(res)
        //     // setLoading(false)
        //     // setIsFetched(true)
        //     // @ts-ignore
        //     setOutput(res.data.choices[0].message.content)
        // }).catch(error => {
        //     console.log(error)
        //     // setError(error)
        // })
    }


    return (
        <button onClick={handleSubmitClick}>
            {props.btnText}
        </button>
    );
};

export default SubmitBtn;
