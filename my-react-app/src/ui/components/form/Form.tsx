import styled from "styled-components";
import {backgroundColors, textColors} from "../../../theme/theme";

 export const Form = styled.form`
    //background-color: ${backgroundColors.redBg};
    color: ${textColors.textWhite};
    width: 80%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
 `;