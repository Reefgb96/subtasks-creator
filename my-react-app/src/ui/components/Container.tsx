import React from 'react';
import {ContainerProps} from "../../types/props";
import styled from "styled-components";

// const Container = (props: ContainerProps):JSX.Element => {
//     return (<div>
//             {props.children}
//         </div>
//     );
// };


const Container = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`;

export default Container;
