import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import './index.css'
// import { defineConfig } from 'vite';
// import react from '@vitejs/plugin-react';
// import replace from '@rollup/plugin-replace';
// import dotenv from 'dotenv';
//
// dotenv.config();

// export default defineConfig({
//     plugins: [
//         react(),
//         replace({
//             'process.env': JSON.stringify(process.env),
//         }),
//     ],
// });

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(<App />)
